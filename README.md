# Park Easy

A Simple Parking Management Application

## Requirements

For building and running the application you need:

- [JDK 11](https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/downloads-list.html)
- [Maven 3](https://maven.apache.org)

## Building Application
You can use the below command to build the application:

```shell
./build.sh
```

## Running the application locally

There are several ways to run this application on your local machine. One way is to execute the `main` method in the `com.park_easy.Application` class from your IDE and providing input file path as arguments

Alternatively you can use below command to run the application::

```shell
./run.sh [INPUT_FILE_PATH]
```
For example:

```shell
./run.sh /e/GitLab/park-easy/input.txt
```
## Testing Application
You can use the below command to run unit tests:

```shell
./test.sh
```