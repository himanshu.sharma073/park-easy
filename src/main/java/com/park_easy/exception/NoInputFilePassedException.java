package com.park_easy.exception;

public class NoInputFilePassedException extends RuntimeException {
	
	public NoInputFilePassedException() {
		super("No Input File Passed");
	}

}
