package com.park_easy.handler;

import java.util.HashMap;
import java.util.Map;

public enum Action {

	CREATE_PARKING_LOT("Create_parking_lot"),
	PARK("Park"),
	LEAVE("Leave"),
	SLOT_NUMBERS_FOR_DRIVER_OF_AGE("Slot_numbers_for_driver_of_age"),
	SLOT_NUMBERS_FOR_CAR_WITH_NUMBER("Slot_number_for_car_with_number"),
	VEHICLE_REGISTRATION_NUMBER_FOR_DRIVER_OF_AGE("Vehicle_registration_number_for_driver_of_age");

	private String action;

	Action(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	private static final Map<String, Action> lookup = new HashMap<String, Action>();

	static {
		for (Action action : Action.values()) {
			lookup.put(action.getAction(), action);
		}
	}

	public static Action get(String url) {
		return lookup.get(url);
	}

}
