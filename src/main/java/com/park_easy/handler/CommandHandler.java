package com.park_easy.handler;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.park_easy.messages.Messages;
import com.park_easy.model.Car;
import com.park_easy.util.Utils;

public class CommandHandler {

	private List<Car> parkedCars;
	private Queue<Integer> availableSlots;

	public String handle(String command) {
		String[] instructions = command.split(" ");
		String result = null;
		Action action = Action.get(instructions[0]);
		if (action == null) {
			return Messages.ACTION_NOT_SUPPORTED;
		}
		switch (action) {
		case CREATE_PARKING_LOT:
			int capacity = Integer.parseInt(instructions[1]);
			result = createParking(capacity);
			break;
		case PARK:
			String regNo = instructions[1];
			int driverAge = Integer.parseInt(instructions[3]);
			result = parkCar(regNo, driverAge);
			break;
		case LEAVE:
			int slot = Integer.parseInt(instructions[1]);
			result = vacantSlot(slot);
			break;
		case SLOT_NUMBERS_FOR_DRIVER_OF_AGE:
			driverAge = Integer.parseInt(instructions[1]);
			result = Utils.listToCommaSeparatedString(getSlotsForDriversWithAge(driverAge));
			break;
		case VEHICLE_REGISTRATION_NUMBER_FOR_DRIVER_OF_AGE:
			driverAge = Integer.parseInt(instructions[1]);
			result = Utils.listToCommaSeparatedString(getVehicleRegistrationNumberForDriversWithAge(driverAge));
			break;
		case SLOT_NUMBERS_FOR_CAR_WITH_NUMBER:
			regNo = instructions[1];
			result = Utils.listToCommaSeparatedString(getSlotsForCarWithNumber(regNo));
			break;
		}
		return result;
	}

	private List<String> getVehicleRegistrationNumberForDriversWithAge(int driverAge) {
		return IntStream.range(0, parkedCars.size()).filter(i -> {
			Car car = parkedCars.get(i);
			return car != null && car.getDriverAge() == driverAge;
		}).mapToObj(i -> parkedCars.get(i).getRegistrationNumber()).collect(Collectors.toList());
	}

	private List<Integer> getSlotsForCarWithNumber(String regNo) {
		return IntStream.range(0, parkedCars.size()).filter(i -> {
			Car car = parkedCars.get(i);
			return car != null && car.getRegistrationNumber().equals(regNo);
		}).mapToObj(i -> i + 1).collect(Collectors.toList());
	}

	private List<Integer> getSlotsForDriversWithAge(int driverAge) {
		return IntStream.range(0, parkedCars.size()).filter(i -> {
			Car car = parkedCars.get(i);
			return car != null && car.getDriverAge() == driverAge;
		}).mapToObj(i -> i + 1).collect(Collectors.toList());
	}

	private String vacantSlot(int slot) {
		if (parkedCars.get(slot - 1) == null) {
			return Messages.SLOT_ALREADY_VACANT;
		}
		Car car = parkedCars.get(slot - 1);
		parkedCars.set(slot - 1, null);
		availableSlots.add(slot);
		return MessageFormat.format(Messages.CAR_LEFT, slot, car.getRegistrationNumber(), car.getDriverAge());
	}

	private String parkCar(String regNo, int driverAge) {
		if (availableSlots.isEmpty()) {
			return Messages.PARKING_FULL;
		}
		Car car = new Car(regNo, driverAge);
		int availableSlot = availableSlots.poll();
		parkedCars.add(availableSlot - 1, car);
		return MessageFormat.format(Messages.CAR_PARKED, regNo, availableSlot);
	}

	private String createParking(int capacity) {
		parkedCars = new ArrayList<Car>(capacity);
		availableSlots = new PriorityQueue<Integer>(
				IntStream.rangeClosed(1, capacity).boxed().collect(Collectors.toList()));
		return MessageFormat.format(Messages.CREATED_PARKING, capacity);
	}

}
