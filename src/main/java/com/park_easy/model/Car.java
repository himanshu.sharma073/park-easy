package com.park_easy.model;

public class Car {

	private String registrationNumber;
	private int driverAge;

	public Car(String registrationNumber, int driverAge) {
		super();
		this.registrationNumber = registrationNumber;
		this.driverAge = driverAge;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public int getDriverAge() {
		return driverAge;
	}	

}
