package com.park_easy.messages;

public class Messages {

	// Successful Action Messages
	public static final String CREATED_PARKING = "Created parking of {0} slots";
	public static final String CAR_PARKED = "Car with vehicle registration number \"{0}\" has been parked at slot number {1}";
	public static final String CAR_LEFT = "Slot number {0} vacated, the car with vehicle registration number \"{1}\" left the space, the driver of the car was of age {2}";
	
	// Failed Action Messages
	public static final String ACTION_NOT_SUPPORTED = "Action Not Supported";
	public static final String PARKING_FULL = "Parking Full";
	public static final String SLOT_ALREADY_VACANT = "Slot already vacant";


}
