package com.park_easy.processor;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import com.park_easy.handler.CommandHandler;

public class FileProcessor {

	private static FileProcessor INSTANCE;

	private FileProcessor() {

	}

	public static FileProcessor getInstance() {
		if (INSTANCE == null) {
			synchronized (FileProcessor.class) {
				if (INSTANCE == null) {
					INSTANCE = new FileProcessor();
				}
			}
		}
		return INSTANCE;
	}

	public void process(String inputFilePath, String outputFilePath) {
		try (Stream<String> in = Files.lines(Path.of(inputFilePath));
				FileWriter out = new FileWriter(outputFilePath);) {
			CommandHandler handler = new CommandHandler();
			in.forEach(command -> {
				try {
					out.write(handler.handle(command));
					out.append('\n');
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
