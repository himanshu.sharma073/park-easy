package com.park_easy.util;

import java.util.List;

public class Utils {

	public static String OUTPUT_FILE_RE = "([\\/\\\\])(.*)\\.txt";
	public static String OUTPUT_FILE_PATTERN = "$1$2_output.txt";
	public static String LIST_TO_COMMA_SEPARATED_OUTPUT_RE = "[\\[\\]\\s]";

	public static String generateOutputFilePath(String inputFilePath) {
		return inputFilePath.replaceAll(OUTPUT_FILE_RE, OUTPUT_FILE_PATTERN);
	}

	public static String listToCommaSeparatedString(List list) {
		return list.toString().replaceAll("[\\[\\]\\s]", "");
	}
}
