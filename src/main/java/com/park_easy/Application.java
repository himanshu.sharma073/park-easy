package com.park_easy;

import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.park_easy.exception.NoInputFilePassedException;
import com.park_easy.processor.FileProcessor;
import com.park_easy.util.Utils;

/**
 * 
 * @author Himanshu 
 * Main class to run Application
 *
 */
public class Application {

	private static Logger logger = Logger.getLogger(Application.class.getName());

	/**
	 * 
	 * @param args args[0] - Input File Path
	 */
	public static void main(String[] args) {
		if (args == null || args.length == 0 || StringUtils.isBlank(args[0])) {
			logger.severe("No Input file passed. Exiting...");
			throw new NoInputFilePassedException();
		}
		String inputFilePath = args[0];
		String outputFilePath = Utils.generateOutputFilePath(inputFilePath);
		logger.info("InputFile: " + inputFilePath);
		FileProcessor.getInstance().process(inputFilePath, outputFilePath);
		logger.info("Processing Complete");
		logger.info("OutputFile: " + outputFilePath);
	}

}
