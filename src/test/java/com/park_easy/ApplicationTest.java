package com.park_easy;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.park_easy.exception.NoInputFilePassedException;
import com.park_easy.util.Utils;
import com.park_easy.utils.FileUtils;

public class ApplicationTest {

	private static final String SAMPLE_INPUT_FILE_PATH = FileUtils.getResourceFilePath("sample_input.txt");
	private static final String GENERATED_OUTPUT_FILE_PATH = Utils.generateOutputFilePath(SAMPLE_INPUT_FILE_PATH);

	@Before
	public void before() throws IOException {
		FileUtils.deleteFile(GENERATED_OUTPUT_FILE_PATH);
	}

	@Test
	public void shouldGenerateAnOutputFile() throws IOException {
		Application.main(new String[] { SAMPLE_INPUT_FILE_PATH });
		List<String> sampleOutput = FileUtils.getFileContent("sample_output.txt");
		List<String> generatedOutput = FileUtils.getFileContentFromPath(GENERATED_OUTPUT_FILE_PATH);
		assertThat(generatedOutput, is(sampleOutput));
	}

	@Test(expected = NoInputFilePassedException.class)
	public void shouldThrowAnExceptionIfNoInputFilePassed() throws IOException {
		Application.main(new String[] {});
	}

	@After
	public void after() throws IOException {
		FileUtils.deleteFile(Utils.generateOutputFilePath(SAMPLE_INPUT_FILE_PATH));
	}

}
