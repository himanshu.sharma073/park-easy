package com.park_easy.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileUtils {

	public static List<String> getFileContent(String fileName) throws IOException {
		return getFileContentFromPath(getResourceFilePath(fileName));
	}
	
	public static List<String> getFileContentFromPath(String path) throws IOException {
		return Files.readAllLines(Path.of(path));
	}
	
	public static String getResourceFilePath(String fileName) {
		ClassLoader classLoader = FileUtils.class.getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		return file.getAbsolutePath();
	}
	
	public static boolean deleteFile(String fileName) throws IOException {
		return Files.deleteIfExists(Paths.get(fileName));
	}

}
