#!/bin/bash

mvn compile exec:java -Dexec.mainClass="com.park_easy.Application" \
  -Dexec.args="$1"